Google Chrome extension for NuCypher network

Description: "While the javascript version of NuCypher isn't yet there, it is still possible to interact with NuCypher network
using the Python code from browsers. For that one needs to use protocol called ""Native messaging"".

Opportunities of my project:
- start NuCypher network connection with chrome extension
- requests to Alice 
- get Alice's grants for data access
- logging information with native messaging protocol
- simple dashboard which can show information about user and real-time chart with known nodes

How to run extension (tested on Linux Ubuntu):
After git clone:
- cd n2/nucypher
- pipenv install --dev --three --skip-lock --pre
After that:
- sudo MyApp/install_host.sh
- at google chrome: in developer mode load unpacking extension from dir with manifest.json (MyApp)
- run extension from google-chrome apps

!!! in file com.google.chrome.example.echo.json you need edit run.sh script's path on your custom 
"path": "$YOUR_LOCATION/n2/nucypher/run.sh"

You can use docker for run host, you need to build image from dockerfile at n2/nucypher and edit run.sh:
change strings "pipenv ..." on "docker run -i $IMAGE_NAME"

Extension interface:
- connect button - run the connetion for nucypher network & we can see data access policy, available files & messages with shared data
- close connection button - close connection and delete access for data
- logging - log informaion from nodes.py (realised with native messaging protocol)
- network - get information about network + real-time dashboard (we can see how many known nodes we have at the moment)

This interface can be used not only in the extension for, but it can be used for solving task №3 (GUI interface for the NuCypher network) and some techniques can be used in task №10 (Mandatory access logging). 